/**
 * @file
 */

(function ($) {
  Drupal.behaviors.quickSearch = {
    attach: function (context, settings) {
      if (typeof Drupal.settings.quick_search_config_options != 'undefined') {
        var element_selector = Drupal.settings.quick_search_config_options.id_element;
        var config_options = Drupal.settings.quick_search_config_options.options;
        
        $('#'+ element_selector).hideseek(config_options);
      }
    }
  };
})(jQuery);
