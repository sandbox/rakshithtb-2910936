<?php
/**
 * @file quick_search.admin.inc
 * admin/config/quick_search form constructor
 */

/**
 * Returns the Quick Search settings form.
 */
function quick_search_settings_form() {
  print $content = '<input id="quick_search" name="search" placeholder="Start typing here" data-list=".default_list" autocomplete="off" type="text"/><ul class="default_list"><li>test one</li><li>test two</li></ul>';
  $form['quick_search_settings'] = array (
    '#type' => 'fieldset',
    '#title' => t('Quick Search Specific Settings')
  );
  $form['quick_search_settings']['quick_search_idelement']= array(
    '#type' => 'textfield',
    '#title'=> t('Input ID selector'),
    '#description' => t('The Input ID selector for which Plugin will be initialized'),
    '#default_value' => t(variable_get('quick_search_idelement', 'quick_search'))
  );
  $form['quick_search_settings']['quick_search_navigation']= array(
    '#type' => 'checkbox',
    '#title'=> t('Navigation'),
    '#description' => t('Search for Hyperlink text'),
    '#default_value' => variable_get('quick_search_navigation', 0),  
  );
  $form['quick_search_settings']['quick_search_hidden_mode']= array(
    '#type' => 'checkbox',
    '#title'=> t('Hidden Mode'),
    '#description' => t('Search for text in Hidden mode'),
    '#default_value' => variable_get('quick_search_hidden_mode', 0)  
  );
  $form['quick_search_settings']['quick_search_nodata']= array(
    '#type' => 'textfield',
    '#title'=> t('No Data Found'),
    '#description' => t('The message to display when no Search results found'),
    '#default_value' => t(variable_get('quick_search_nodata', ''))
  );
  $form['quick_search_settings']['quick_search_headers']= array(
    '#type' => 'textfield',
    '#title'=> t('Headers Class Name'),
    '#description' => t('The class name for the The parent item text to be shown with the searched text'),
    '#default_value' => t(variable_get('quick_search_headers', ''))  
  );
  $form['quick_search_settings']['highlight_related_settings'] = array (
    '#type' => 'fieldset',
    '#title' => t('Highlight Text Related Settigs'),
  );
  $form['quick_search_settings']['highlight_related_settings']['quick_search_highlight']= array(
    '#type' => 'checkbox',
    '#title'=> t('Highlight'),
    '#description' => t('Highlight the search term'),
    '#default_value' => variable_get('quick_search_highlight', 0)  
  );
  $form['quick_search_settings']['highlight_related_settings']['quick_search_ignore']= array(
    '#type' => 'textfield',
    '#title'=> t('Ignore Class Name'),
    '#description' => t('The Class Name for the item to be ignored from Highlighting'),
    '#states' => array (
      "visible" => array(
        "input[name='quick_search_highlight']" => array("checked" => TRUE)
      ),
    ),
    '#default_value' => t(variable_get('quick_search_ignore', ''))  
  );
  $form['quick_search_settings']['highlight_related_settings']['quick_search_ignore_accents']= array(
    '#type' => 'checkbox',
    '#title'=> t('Ignore Accents'),
    '#description' => t('Ignore Accents'),
    '#states' => array (
      "visible" => array(
        "input[name='quick_search_highlight']" => array("checked" => TRUE)
      ),
    ),
    '#default_value' => variable_get('quick_search_ignore_accents', 0)  
  );
  
  //Save all configuration info in variables table
  $form = system_settings_form($form);
  return $form;
}