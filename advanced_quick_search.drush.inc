<?php

/**
 * @file
 *   drush integration for views_infinite_scroll.
 */

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function advanced_quick_search_drush_command() {
  $items = array();

  $items['dl-quicksearch-plugin'] = array(
    'callback' => 'drush_quick_search_hideseek_download',
    'description' => dt('Downloads the required hideseek plugin for quick search.'),
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function advanced_quick_search_drush_help($section) {
  switch ($section) {
    case 'drush:quicksearch-plugin':
      return dt('Downloads the required hideseek plugin for quick search.');
  }
}

/**
 * Example drush command callback.
 *
 * This is where the action takes place.
 *
 * In this function, all of Drupals API is (usually) available, including
 * any functions you have added in your own modules/themes.
 *
 * To print something to the terminal window, use drush_print().
 *
 */
function drush_quick_search_hideseek_download() {
  if(module_exists('libraries')) {
    $path = 'sites/all/libraries/hideseek';

    // Create the path if it does not exist.
    if (!is_dir($path)) {
      drush_op('mkdir', $path);
      drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
    }
  }

  drush_op('chdir', $path);
  $libraries = libraries_info('hideseek');

  if (!empty($libraries)) {
    $download_uri = $libraries['recommended']['uri'];
    // Download the plugin.
    if (drush_shell_exec("wget https://raw.githubusercontent.com/vdw/HideSeek/master/jquery.hideseek.min.js")) {
      drush_log(dt('hideseek Plugin has been downloaded to @path', array('@path' => $path)), 'success');
    }
    else {
      drush_log(dt('Drush was unable to download hideseek Plugin to @path', array('@path' => $path)), 'error');
    }
  }
}
